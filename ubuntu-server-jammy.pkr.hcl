# Ubuntu Server jammy
# ---
# Packer Template to create an Ubuntu Server (jammy) on Proxmox

# Variable Definitions


# Resource Definiation for the VM Template
packer {
  required_plugins {
    proxmox = {
      version = " >= 1.1.0"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

source "proxmox" "ubuntu-server-jammy" {

  proxmox_url      = var.proxmox_api_url
  username         = var.proxmox_api_token_id
  token            = var.proxmox_api_token_secret
  node             = "hv1"
  vm_id            = "9006"
  vm_name          = "Hashi-ubuntu-server-jammy-1"
  iso_url          = "https://releases.ubuntu.com/22.04/ubuntu-22.04.1-live-server-amd64.iso"
  iso_checksum     = "10f19c5b2b8d6db711582e0e27f5116296c34fe4b313ba45f9b201a5007056cb"
  iso_storage_pool = "ISO_Storage" # Specify your storage pool

  ssh_username = "ubuntu"
  ssh_password = "ubuntu"
  ssh_timeout  = "20m"

  cloud_init              = true
  cloud_init_storage_pool = "pool1"

  boot      = "c"
  boot_wait = "5s"
  boot_command = [
    "<esc><wait>",
    "e<wait>",
    "<down><down><down><end>",
    "<bs><bs><bs><bs><wait>",
    "autoinstall ds=nocloud-net;s=/cidata/ ---<wait>",
    "<f10><wait>"
  ]

  #"autoinstall ds=nocloud-net\\;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ---<wait>",
  # "autoinstall ds=nocloud-net;s=/cidata/ ---<wait>",
  # PACKER Autoinstall Settings
  #  http_directory = "http" 
  # (Optional) Bind IP Address and Port
  #  http_bind_address = "0.0.0.0"
  # http_port_min = 8802
  # http_port_max = 8802

  insecure_skip_tls_verify = true

  template_name        = "Hashi-ubuntu-server-jammy"
  template_description = "Packer generated Ubuntu 22.04 with nomad, consul, and vault"
  unmount_iso          = true

  additional_iso_files {
    cd_files = [
      "./http/meta-data",
      "./http/user-data"
    ]
    cd_label         = "cidata"
    iso_storage_pool = "ISO_Storage"
  }

  memory     = "16384"
  cores      = "8"
  sockets    = "1"
  os         = "l26"
  qemu_agent = true

  scsi_controller = "virtio-scsi-pci"

  disks {
    disk_size         = "20G"
    format            = "raw"
    storage_pool      = "pool1" #Specify your storage pool
    storage_pool_type = "zfs"   #Specify pool type
    type              = "virtio"
  }


  network_adapters {
    model    = "virtio"
    bridge   = "vmbr0"
    firewall = "false"
  }
}


# Build Definition to create the VM Template
build {

  name    = "Hashi-ubuntu-server-jammy"
  sources = ["source.proxmox.ubuntu-server-jammy"]

  # Provisioning the VM Template for Cloud-Init Integration in Proxmox #1
  provisioner "shell" {
    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
      "sudo rm /etc/ssh/ssh_host_*",
      "sudo truncate -s 0 /etc/machine-id",
      "sudo apt -y autoremove --purge",
      "sudo apt -y clean",
      "sudo apt -y autoclean",
      "sudo cloud-init clean",
      "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
      "sudo sync"
    ]
  }

  provisioner "shell" {
    inline = [
      "sudo wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg",
      "echo deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main | sudo tee /etc/apt/sources.list.d/hashicorp.list",
      "sudo apt update",
      "sudo apt install nomad -y",
      "sudo apt install consul -y",
      "sudo apt install vault -y",
    ]
  }

  # Provisioning the VM Template for Cloud-Init Integration in Proxmox #2
  provisioner "file" {
    source      = "99-pve.cfg"
    destination = "/tmp/99-pve.cfg"
  }

  # Provisioning the VM Template for Cloud-Init Integration in Proxmox #3
  provisioner "shell" {
    inline = ["sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg"]
  }

  # Add additional provisioning scripts here
  # ...
}


