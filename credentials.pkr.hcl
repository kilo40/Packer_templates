variable "proxmox_api_url" {
  description = "Proxmox API url"
  type        = string

}

variable "proxmox_api_token_id" {
  description = "Proxmox API token ID"
  type        = string

}


variable "proxmox_api_token_secret" {
  description = "Proxmox API token secret"
  sensitive   = true
  type        = string

}

